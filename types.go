//
// A basic package that facilitates interactions with the Cyclops
// customer db - these types are used within the customer DB itself
// as well as clients of the customer DB.
//
// (C) Cyclops Labs 2019
//
package types

type Product struct {
	ProductId   string `json:"productId"`
	ResourceId  string `json:"resourceId"`
	ProjectId   string `json:"projectId"`
        Fqdn        string `json:"fqdn"`
	CustomerId  string `json:"customerId"`
	BelongsTo   string `json:"belongsTo"`
	Name        string `json:"name"`
	Type        string `json:"type"`
	Discount    string `json:"discount"`
	CancelDate  string `json:"cancelDate"`
	Billable    string `json:"billable"`
	IsActive    string `json:"isActive"`
	Description string `json:"description"`
	ApiLink     string `json:"apiLink"`
}

type OSProjectObject struct {
	ProjectName   string `json:"projectName"`
	ProjectID     string `json:"projectId"`
	LdapID        string `json:"ldapId"`
	Billable      string `json:"billable"`
	IsActive      string `json:"isActive"`
	IsIaaSProject string `json:"isIaasProject"`
}

type Customer struct {
	CustomerId   string            `json:"customerId"`
	ResellerId   string            `json:"resellerId"`
	Name         string            `json:"name"`
	Address      string            `json:"address"`
	BillContact  string            `json:"billContact"`
	Discount     string            `json:"discount"`
	BillPeriod   string            `json:"billPeriod"`
	BillCurrency string            `json:"billCurrency"`
	InvoiceMode  string            `json:"invoiceMode"`
	EmailTo      string            `json:"emailTo"`
	EmailCc      string            `json:"emailCc"`
	EmailBcc     string            `json:"emailBcc"`
	IsActive     string            `json:"isActive"`
	CancelDate   string            `json:"cancelDate"`
	Gender       string            `json:"gender"`
	Language     string            `json:"language"`
	Products     []Product         `json:"products"`
	ApiLink      string            `json:"apiLink"`
	Billable     string            `json:"billable"`
	OSProjects   []OSProjectObject `json:"osProjects"`
}

type Reseller struct {
	ResellerId   string            `json:"resellerId"`
	Name         string            `json:"name"`
	Address      string            `json:"address"`
	BillContact  string            `json:"billContact"`
	Discount     string            `json:"discount"`
	BillPeriod   string            `json:"billPeriod"`
	BillCurrency string            `json:"billCurrency"`
	InvoiceMode  string            `json:"invoiceMode"`
	EmailTo      string            `json:"emailTo"`
	EmailCc      string            `json:"emailCc"`
	EmailBcc     string            `json:"emailBcc"`
	IsActive     string            `json:"isActive"`
	CancelDate   string            `json:"cancelDate"`
	Gender       string            `json:"gender"`
	Language     string            `json:"language"`
	Customers    []Customer        `json:"customers"`
	ApiLink      string            `json:"apiLink"`
	Billable     string            `json:"billable"`
	DomainId     string            `json:"domainId"`
	IsIaaSDomain string            `json:"isIaasDomain"`
	OSProjects   []OSProjectObject `json:"osProjects"`
}
