//
// A basic package that facilitates interactions with the Cyclops
// customer db - these types are used within the customer DB itself
// as well as clients of the customer DB.
//
// (C) Cyclops Labs 2019
//

package client

import (
	"bytes"
	"encoding/json"
	l "gitlab.com/cyclops-utilities/logging"
	"gitlab.com/cyclops-utilities/types"
	"io/ioutil"
	"net/http"
)

// CustomerDBService contains the endpoint where the customer db service lives...
// It is implemented as an interface which includes a PostCommand...
type CustomerDBService struct {
	Endpoint string
}

// Creates a Reseller in the customer DB; note that it is assumed that the
// reseller contains a set of customers and that these can be marshalled
// correctly.
func (s *CustomerDBService) CreateReseller(r types.Reseller) (success bool, err error) {
	val, _ := json.Marshal(r)
	// fmt.Printf("r= %+v\n", string(val))
	newCustomerEndpoint := s.Endpoint + "/reseller/"
	req, err := http.NewRequest("POST", newCustomerEndpoint, bytes.NewBuffer(val))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		//panic(err)
		l.Error.Printf("error = %v\n", err.Error())
		success = false
		return
	}

	//TODO(murp) - fixme...)
	defer resp.Body.Close()

	if err == nil {
		if resp.StatusCode != http.StatusCreated {
			l.Warning.Printf("Unexpected response Creating new Reseller record - Response Status: %v\n", resp.Status)
			body, _ := ioutil.ReadAll(resp.Body)
			l.Debug.Printf("Response Body: %v\n", string(body))
			success = false
		} else {
			// this is the successful case - only dump output if in debug mode as this
			// should be the default...
			body, _ := ioutil.ReadAll(resp.Body)
			l.Debug.Printf("Sending Cyclops Customer DB service - Response Status: %v, Response Body: %v\n", resp.Status, string(body))
			success = true
		}
	}
	return
}
